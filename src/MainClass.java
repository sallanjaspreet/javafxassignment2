import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainClass extends Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		// 1. Create & configure user interface controls

		// Location

		Label headingLabel = new Label("Airport Ride Calculator");
		Label locationLabel = new Label("From:");

		ObservableList<String> items = FXCollections.observableArrayList("Brampton", "Cestar College");
		ComboBox<String> locationCombo = new ComboBox<String>(items);
		// Options

		CheckBox checkExtraLuggage = new CheckBox("Extra Luggage?");
		CheckBox cbPets = new CheckBox("Pets?");
		CheckBox use407 = new CheckBox("Use 407 ETR?");
		CheckBox addTip = new CheckBox("Add Tip?");
		Button calculate = new Button("Calculate");
		Label lblResult = new Label("");

		calculate.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				double total = 0;
				double extraluggage = 10;
				double pets = 6;
				double ETR = 0.25;
				double taxR = 0.13;
				double baseFare = 0;
				double distance = 0;
				// Check for the selected value in combo box
				if (locationCombo.getValue().contentEquals("Brampton")) {
					baseFare = 38;
					distance = 30;
				} else {
					baseFare = 51;
					distance = 20;
				}

				double additionalFare = 0;

				if (checkExtraLuggage.isSelected()) {
					
					additionalFare += extraluggage;

				}

				if (cbPets.isSelected()) {
					
					additionalFare += pets;

				}
				if (use407.isSelected()) {
					
					additionalFare += (distance * ETR);

				}
				
				
				//Tax
				double tax = (baseFare + additionalFare) * taxR;
				
				total = baseFare + additionalFare + tax;
				
				if(addTip.isSelected()) {
					double tip = total * 0.15;
					total += tip;
				}
				
				lblResult.setText("The total fare is: $"+ total);

			}
		});

		// 2. Make a layout manager
		VBox root = new VBox();
		// 3. Add controls to the layout manager
		root.getChildren().add(headingLabel);
		root.getChildren().add(locationLabel);
		root.getChildren().add(locationCombo);
		root.getChildren().add(checkExtraLuggage);
		root.getChildren().add(cbPets);
		root.getChildren().add(use407);
		root.getChildren().add(addTip);
		root.getChildren().add(calculate);
		root.getChildren().add(lblResult);
		// 4. Add layout manager to scene & Add scene to a stage
		root.setPadding(new Insets(10, 100, 10, 100));
		root.setSpacing(10);
		primaryStage.setTitle("Fare Calculator");
		primaryStage.setScene(new Scene(root, 500, 300));
		// 6. Show the app
		primaryStage.show();
	}

}
